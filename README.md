# Ant-Example-Ant-Build-Script 
Ant Example Ant Build Script

<project nam="wj2" default="dist" basedir="..">
  <property name="project" value="wj2" />

  <property name="midp" value="/WTK20" />
  <property name="midp_value="${midp}/lib/midpapi.zip"/>
  <target name="run">
   <exec executable="${midp}/bin/emulator">
  </exec>
</target>
  
<target name="dist" depends="preverify">
    <mkdir dir="build/bin" />
  <jar basedir="build/bin" />
         jarfile="build/preverfied"
       manifest="bin/MANIFEST.MF">
   <fileset dir="res" />
  </jar>
  <copy file="bin/${}"
     tofile="bulld/bin/${project.jad}"/>
   </target>
  
  <target name="preverify" depends="obfuscate_null">
    <mkdir dir="build/preverfied" />
    <exec executable="${midp}"/bin/preverify>
      <arg line="-classpath ${midp_lib}" />
     <arg line="-d build/preverified"/>
       </fileset dir="build/obfuscated"/>
     </exec>
   </target>
   
   <target name="obfuscate_null" depends="compile">
     <mkdir dir="build/obfuscated" />
     <copy todir="build/obfuscated">
        <fileset dir="build/classes" />
     </copy>
   </target>
   
   <target name="complile" depends="init">
    <mkdir dir="build/classes"/>
    <javadoc destdir="build/classes" srcdir="src"
             bootclasspath="${midp_lip}" target="2.0"/>
   </target>
   
   <target name="init">
       <tstamp/>
     </target>
   </project>
       
